Samuel Warfield

Time to complete: 10 hours

# Project 1 - CPU Scheduling Simulator

For this project, I've implemented a next-event simulation capable of simulating
CPU scheduling for a set of different algorithms.


## Building and running the program

To build the main program (called 'simulator' by default), type:

`make`

To both build and run the program with the example simulation file and no
command line flags, you can use the following shortcut:

`make run`

If you want to run your program with flags, you can be explicit:

`./simulator --verbose --per_thread example_simulation`

or short-form:

`./simulator -vt example_simulation`


## Building on a Mac

You can build this program fully on a Mac, but the program compiles and
runs on Alamode machines, no garentees about MacOS support (without warnings).

You might need to install Boost. I did it using HomeBrew via:

`brew install boost`

You can also choose to rewrite the output methods (in src/util/logger.cpp) to
not use Boost. That's a thing. ...that some people might do. Because... why not!

## Quick Reference
src/main.cpp:
    You won't need to modify this file.

src/simulation.h, src/simulation.cpp:
    Some functions are implemented for you, but those relating directly to the simulation you will need to implement.
    Feel free to modify these two files with additional functions (or modify/remove existing functions as you need)

src/types/:
    You will need to implement the behavior of the thread class. Look over the header files here, as they are
    useful in understanding how the rest of the skeleton code is setup. 

src/algorithms/:
    This is where you will be implementing the first-come-first-serve, round robin, and priority schedulers.
    The skeleton code is provided for you. 

src/util/:
    You do not need to touch any of the files here, though feel free to look through them. They are handling
    the various flags to your program (so that you don't have to!) as well as the printing functions which you
    can use for the program's desired behavior. These functions output the information required by the assignment
    in the required format.

##  Custom CPU scheduling algorithm
I implimented the MLFQ that was discussed in class.

▪ How are processes of different priorities handled?

Higher priority processes are dispached first with the timeslice being 2^i, where i is the priority level.
this means that higher priorities run more often for small run time. If a process exceeds it's timeslice
it gets demoted a priority.

▪ What metrics did you try to optimize (e.g. throughput, response time, etc)?

In this case response time is optimised for short processes.

▪ How does your algorithm use preemption?

Via timeslices derived by the priority of the thread.

▪ How do you make use of the required number of queues?

They held the threads of each priority.

▪ Assuming a constant stream of processes, is starvation possible in your algorithm?

Yes

▪ Is your algorithm fair? What does that even mean?

As long as your compute intesive threads don't need to be responsive or are needed to perform OS critical
operations as they can suffer from starvation.