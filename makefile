# To build the program (which is called simulator by default), simply type:
#     make
#
# To clean up and remove the compiled binary and other generated files, type:
#     make clean
#
# To build AND run the simulator, type:
#     make run
#

# The name of your binary.
NAME = simulator

# Flags passed to the preprocessor.
CPPFLAGS += -Werror -MMD -MP -Isrc -g -std=c++11

# ALL .cpp files.
SRCS = $(shell find src -name '*.cpp')
OBJS = $(SRCS:src/%.cpp=bin/%.o)
DEPS = $(SRCS:src/%.cpp=bin/%.d)

.PHONY: test test_*

# Default target.
$(NAME): $(OBJS)
	$(CXX) $(CPP_FLAGS) $^ -o $(NAME)

# Build and run the program.
run: $(NAME)
	./$(NAME) test_files/example_simulation

# Build and test the programe
test: $(NAME) test_PRIORITY
test_FCFS:
	echo; echo "TESTING FCFS";\
	 ./$(NAME) -tv test_files/example_simulation | diff -ws test_files/example_simulation_output_fcfs_tv_flags_v2 - \
	#|| sl \
	echo

test_RR: test_FCFS
	echo; echo "TESTING RR"; \
	./$(NAME) -tva RR test_files/example_simulation | diff -ws test_files/example_simulation_rr_tv_flags -; \
	echo

test_PRIORITY: test_MLFQ

	echo; echo "TESTING PRIORITY"; \
	./$(NAME) -tva PRIORITY test_files/example_simulation | diff -ws test_files/example_simulation_priority_tv_flags -; \
	echo

test_MLFQ: test_RR

	echo; echo "TESTING MLFQ"; \
	./$(NAME) -tva MLFQ test_files/example_simulation | diff -ws test_files/example_simulation_mlfq_tv_flags -; \
	echo

# Remove all generated files.
clean:
	rm -rf $(NAME)* bin/

# Ensure the bin/ directories are created.
$(SRCS): | bin

# Mirror the directory structure of src/ under bin/
bin:
	mkdir -p $(shell find src -type d | sed "s/src/bin/")

# Build objects.
bin/%.o: src/%.cpp
	$(CXX) $(CPPFLAGS) $< -c -o $@

# Auto dependency management.
-include $(DEPS)
