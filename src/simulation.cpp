#include "simulation.h"
#include "types/event.h"
#include <cassert>
#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

void Simulation::run(const string& filename) {
    read_file(filename);

    // While their are still events to process, invoke the corresponding methods
    // to handle them.
    while (!events.empty()) {
        const Event* event = events.top();
        events.pop();

        // Invoke the appropriate method on the scheduler for the given event type.
        switch (event->type) {
        case Event::THREAD_ARRIVED:
            handle_thread_arrived(event);
            break;

        case Event::THREAD_DISPATCH_COMPLETED:
            handle_thread_dispatch_completed(event);
            break;

        case Event::PROCESS_DISPATCH_COMPLETED:
            handle_process_dispatch_completed(event);
            break;

        case Event::CPU_BURST_COMPLETED:
            handle_cpu_burst_completed(event);
            break;

        case Event::IO_BURST_COMPLETED:
            handle_io_burst_completed(event);
            break;

        case Event::THREAD_COMPLETED:
            handle_thread_completed(event);
            break;

        case Event::THREAD_PREEMPTED:
            handle_thread_preempted(event);
            break;

        case Event::DISPATCHER_INVOKED:
            handle_dispatcher_invoked(event);
            break;
        }

        // Free the event's memory.
        delete event;
    }
}


//==============================================================================
// Event-handling methods
//==============================================================================


void Simulation::handle_thread_arrived(const Event* event) {
    // TODO: handle this event properly (feel free to modify code structure, tho)
    // if (this->active_thread == nullptr){then evoke the dispacher}
    // else schedule it
    // invoke the scheduler to do dis
    Thread* cur_thread = event->thread;

    // Update the NEW thread's state
    cur_thread->arrival_time = event->time;
    cur_thread->update_state(Thread::READY, event->time);
    log_thread_change((const Event*)event);
    (*scheduler).enqueue(event, event->thread); // TODO I dont trust this here

    // If Premtable
    if ((*scheduler).should_preempt_on_arrival(event)){
        // Remove the end of cpu burst / upcoming preempt and reschedule NOW
        vector<const Event*> tpmry_event_parking;
        const Event* current_event_to_park;
        
        while (!events.empty()){
            current_event_to_park = events.top();

            // if it is what we are looking for, remove it
            if (current_event_to_park->type == Event::CPU_BURST_COMPLETED || 
                current_event_to_park->type == Event::THREAD_PREEMPTED) {
                    delete current_event_to_park;
                    break;
                }
            
            tpmry_event_parking.push_back(current_event_to_park);
            events.pop();
        }

        // Reconstruct the event queue
        for (auto leaving_drunkard : tpmry_event_parking){
            events.push(leaving_drunkard);
        }

        // Create Preempt event 
        Event* boi = new Event(Event::THREAD_PREEMPTED, event->time, event->thread);
        events.push(boi);
    }
    else if (sim_first_thread) { // Schedule Dis Boi (Might need to change with preempt behaivour)
        sim_first_thread = false;
        events.push(new Event(Event::DISPATCHER_INVOKED, event->time, event->thread));
    }

    //cout << "event: THREAD_ARRIVED" << endl;
}

void Simulation::std_dispatch_completed(const Event* event){
    Thread* cur_thread = event->thread;
    // TODO: handle this event properly (feel free to modify code structure, tho)
    // Set active_thread to cur_thread
    prev_thread = active_thread;
    active_thread = cur_thread;

    // Set the start time if this is it's first time running 
    if (cur_thread->previous_state == Thread::NEW) {
        cur_thread->start_time = event->time;
    }

    // Schedule Burst (Use the scheduler descision Luke)
    Event* next;
    Burst* next_burst = cur_thread->bursts.front();
    SchedulingDecision* scheduler_d;
    int burst_time = event->time;
    size_t time_slice = event->scheduling_decision->time_slice;

    // Calc burst Time
    if (time_slice == -1 || time_slice >= next_burst->length){
        burst_time += next_burst->length;
        next = new Event(Event::CPU_BURST_COMPLETED, burst_time, cur_thread, event->scheduling_decision->create_copy());
    } else {
        // dis boi gets preempted
        next = new Event(Event::THREAD_PREEMPTED, burst_time + time_slice, cur_thread, event->scheduling_decision->create_copy());
    }
        
    // Create Coresponding event
    cur_thread->update_state(Thread::RUNNING, event->time);
    log_thread_change((const Event*)event);
    
    // Push to queue
    events.push(next);
}

void Simulation::handle_thread_dispatch_completed(const Event* event) {
    std_dispatch_completed(event);
}


void Simulation::handle_process_dispatch_completed(const Event* event) {
    std_dispatch_completed(event);
}


void Simulation::handle_cpu_burst_completed(const Event* event) {
    Thread* cur_thread = event->thread;

    // Update Thread State
    cur_thread->service_time += event->time - cur_thread->state_change_time;
    cur_thread->bursts.pop();

    // Update Simulation State
    prev_thread = cur_thread;
    active_thread = nullptr;

    // Check if the Thread is finished
    if (cur_thread->bursts.size() == 0){
        Event* exit = new Event(Event::THREAD_COMPLETED, event->time, cur_thread);
        events.push(exit);
        return;
    }

    // Create IO Event
    Event* io_event;
    io_event = new Event(Event::IO_BURST_COMPLETED, cur_thread->bursts.front()->length + event->time, cur_thread, event->scheduling_decision->create_copy());
    events.push(io_event);

    // Finished current thread so schedule a new one to run
    Event* disp_event;
    disp_event = new Event(Event::DISPATCHER_INVOKED, event->time, nullptr);
    events.push(disp_event);

    cur_thread->update_state(Thread::BLOCKED, event->time);
    log_thread_change((const Event*)event); // ask why this isnt before the Thread Complete
}


void Simulation::handle_io_burst_completed(const Event* event) {
    Thread* cur_thread = event->thread;
    
    // Update Thread state
    cur_thread->io_time += event->time - cur_thread->state_change_time;
    cur_thread->update_state(Thread::READY, event->time);
    log_thread_change((const Event*)event);
    cur_thread->bursts.pop();

    // Enqueue the READY thread
    (*scheduler).enqueue(event, cur_thread);
    if (active_thread == nullptr && events.size() == 0){
        Event* disp_event;
        disp_event = new Event(Event::DISPATCHER_INVOKED, event->time, nullptr);
        events.push(disp_event);
    }

    //cout << "event: IO_BURST_COMPLETED" << endl;
}


void Simulation::handle_thread_completed(const Event* event) {
    // TODO: handle this event properly (feel free to modify code structure, tho)
    Thread* cur_thread = event->thread;

    // Update Thread State
    cur_thread->end_time = event->time; // Time of death recorded
    cur_thread->update_state(Thread::EXIT, event->time);
    log_thread_change((const Event*)event);

    // Update Simulation State
    prev_thread = active_thread;
    active_thread = nullptr;

    // Check if all PIDs have exited
    if (is_sim_done()){
        stats.total_time = event->time;
        calculate_statistics();
        
        for (auto proc : processes){
            logger.print_process_details(proc.second);
        }
        
        logger.print_statistics(stats);
        return;
    }

    // Create New Dispach
    Event* disp_event;
    disp_event = new Event(Event::DISPATCHER_INVOKED, event->time, cur_thread);
    events.push(disp_event);

    //cout << "event: THREAD_COMPLETED" << endl;
}

// This gona be messy
void Simulation::handle_thread_preempted(const Event* event) {
    // TODO: Handle Preempted on arrival
    Thread* cur_thread = event->thread;

    // Update Thread State
    cur_thread->service_time += event->time - cur_thread->state_change_time;
    cur_thread->bursts.front()->length -= event->scheduling_decision->time_slice;

    // Update Simulation State
    prev_thread = cur_thread;
    active_thread = nullptr;

    cur_thread->update_state(Thread::READY, event->time);
       // Finished current thread so schedule a new one to run
    Event* disp_event;
    disp_event = new Event(Event::DISPATCHER_INVOKED, event->time, nullptr);
    events.push(disp_event);
    scheduler->enqueue(event, cur_thread);

    log_thread_change((const Event*)event); // ask why this isnt before the Thread Complete
}

/**
 * The OS needs to schedule a new thread to run, if one is available.
 */
void Simulation::handle_dispatcher_invoked(const Event* event) {

    /*
    Dispatching threads requires a non-zero amount of OS overhead.
        • If the previously executed thread belongs to a different process than the new thread, a full process switch
          occurs. This is also the case for the first thread being executed.
        • If the previously executed thread belongs to the same process as the new thread being dispatched, a
          cheaper thread switch is done.
        • A full process switch includes any work required by a thread switch.
    */

    // Get the Thread that this is releated to and declare some stuff
    int time_to_dispach = event->time;
    Thread* cur_thread;
    Thread* new_thread;
    Event::Type dispatch_type;
    SchedulingDecision* decision;


    

    // Handles if there is no previous or active processes
    if (active_thread == nullptr){
        if (prev_thread == nullptr) {
            cur_thread = event->thread;

            //cur_thread = new Thread(-1, -1, new Process(-1, Process::SYSTEM));
        } else {
            cur_thread = prev_thread;
        }
    }

    // Check if there is an associated sheduling decisions
    if (event->scheduling_decision != nullptr){
        decision = event->scheduling_decision;
    } 
    // Else ask the sheduler what to do
    else {
        decision = (*scheduler).get_next_thread(event);
    }

    if (decision == nullptr){
        return;
    }

    new_thread = decision -> thread;
    // TODO Deal with Nullptr on new Thread

    // get the time to process the the switch
    if (new_thread->process != cur_thread->process){
        time_to_dispach += process_switch_overhead;
        dispatch_type = Event::PROCESS_DISPATCH_COMPLETED;

    } else if (dispatch_first){
        dispatch_first = false;
        time_to_dispach += process_switch_overhead;
        dispatch_type = Event::PROCESS_DISPATCH_COMPLETED;
        
    } else {
        time_to_dispach += thread_switch_overhead;
        dispatch_type = Event::THREAD_DISPATCH_COMPLETED;
    }

    logger.print_verbose(event, new_thread, decision->explanation);

    // add it to the queue
    stats.dispatch_time += time_to_dispach - event->time;
    events.push(new Event(dispatch_type, time_to_dispach, new_thread, decision));
    //cout << "event: DISPATCHER_INVOKED" << endl;
}


//==============================================================================
// Utility methods
//==============================================================================


bool Simulation::is_sim_done() {
    // Go through all Procs and see if all threads have exited
    for (auto proc : processes){
        for (auto thread : proc.second->threads){
            if (thread->current_state != Thread::EXIT) return false;
        } 
    }

    return true;
}

void Simulation::log_thread_change(const Event* event){
    logger.print_state_transition(event, event->thread->previous_state, event->thread->current_state);
}

void Simulation::add_event(Event* event) {
    if (event != nullptr) {
        events.push(event);
    }
}

void Simulation::read_file(const string& filename) {
    ifstream file(filename.c_str());

    if (!file) {
        cerr << "Unable to open simulation file: " << filename << endl;
        exit(EXIT_FAILURE);
    }

    size_t num_processes;

    // Read the total number of processes, as well as the dispatch overheads.
    file >> num_processes >> thread_switch_overhead >> process_switch_overhead;

    // Read in each process.
    for (size_t p = 0; p < num_processes; p++) {
        Process* process = read_process(file);

        processes[process->pid] = process;
    }
}


Process* Simulation::read_process(istream& in) {
    int pid, type;
    size_t num_threads;

    // Read in the process ID, its type, and the number of threads.
    in >> pid >> type >> num_threads;

    // Create the process and register its existence in the processes map.
    Process* process = new Process(pid, (Process::Type) type);

    // Read in each thread in the process.
    for (size_t tid = 0; tid < num_threads; tid++) {
        process->threads.push_back(read_thread(in, tid, process, type));
        // Record thread type
        stats.thread_counts[type]++;
    }

    return process;
}


Thread* Simulation::read_thread(istream& in, int tid, Process* process, int type) {
    int arrival_time;
    size_t num_cpu_bursts;

    // Read in the thread's arrival time and its number of CPU bursts.
    in >> arrival_time >> num_cpu_bursts;

    Thread* thread = new Thread(arrival_time, tid, process, type);

    // Read in each burst in the thread.
    for (size_t n = 0, burst_length; n < num_cpu_bursts * 2 - 1; n++) {
        in >> burst_length;

        Burst::Type burst_type = (n % 2 == 0)
                ? Burst::CPU
                : Burst::IO;

        thread->bursts.push(new Burst(burst_type, burst_length));
    }

    // Add an arrival event for the thread.
    events.push(new Event(Event::THREAD_ARRIVED, thread->arrival_time, thread));

    return thread;
}


void Simulation::calculate_statistics() {
    // TODO: your code here (optional; feel free to modify code structure)
    stats.run_all_calcs(processes);
}
