#include "algorithms/multilevel_feedback_scheduler.h"

using namespace std;


SchedulingDecision* MultilevelFeedbackScheduler::get_next_thread(
        const Event* event) {

    SchedulingDecision* decision = new SchedulingDecision();

    // Choose Thread
    for (int i = 0; i < 4; i++){
        if (!queues[i].empty()){
            decision->thread = queues[i].front();
            queues[i].pop();
            decision->time_slice = pow(2, i);
            decision->explanation = "Selected to run from queue " + to_string(i);
            break;
        }
    }

    if (decision->thread == nullptr){
        delete decision;
        decision = nullptr;
    }

    return decision;
}


void MultilevelFeedbackScheduler::enqueue(const Event* event, Thread* thread) {
    // move to the next queue 
    if(thread->current_queue < 3 && event->type == Event::THREAD_PREEMPTED) {
        thread->current_queue++;
    }

    queues[thread->current_queue].emplace(thread);
}


bool MultilevelFeedbackScheduler::should_preempt_on_arrival(
        const Event* event) const {
    // TODO: implement me
    return false;
}


size_t MultilevelFeedbackScheduler::size() const {
    int size = 0;
    for (int i = 0; i < 4; i++){
        size += queues[i].size();
    }

    return size;
}
