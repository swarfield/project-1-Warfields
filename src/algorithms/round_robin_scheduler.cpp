#include "round_robin_scheduler.h"

using namespace std;


SchedulingDecision* RoundRobinScheduler::get_next_thread(const Event* event) {
    // Is there any Thread Ready?
    if (ready_queue.empty()){
        return nullptr;
    }

    // Generate a New Scheduling decision
    SchedulingDecision* decision = new SchedulingDecision;
    decision -> time_slice = time_slice;

    // Get next thread
    Thread* the_choosen_one = ready_queue.front();
    ready_queue.pop();
    decision->thread = the_choosen_one;

    // Create a explanation
    decision->explanation = "    Selected from ";
    decision->explanation += to_string(this->size() + 1);

    if (decision->time_slice == -1){
        decision->explanation += " threads; will run to completion of burst";
    } else {
        decision->explanation += " threads; will run for at most ";
        decision->explanation += to_string(decision->time_slice);
        decision->explanation += " ticks";
    }

    // Update size()
    return decision;
}


void RoundRobinScheduler::enqueue(const Event* event, Thread* thread) {
    ready_queue.emplace(thread);
}


bool RoundRobinScheduler::should_preempt_on_arrival(const Event* event) const {
    return false;
}


size_t RoundRobinScheduler::size() const {
    return ready_queue.size();
}
