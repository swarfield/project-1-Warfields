#include "algorithms/priority_scheduler.h"

using namespace std;
using boost::format;


SchedulingDecision* PriorityScheduler::get_next_thread(const Event* event) {
    SchedulingDecision* decision = new SchedulingDecision();

    // Choose Thread
    for (int i = 0; i < 4; i++){
        if (!queues[i].empty()){
            decision->thread = queues[i].front();
            queues[i].pop();



            decision->explanation = "Selected from queue " + to_string(i) + ' ' + queue_counts();
            break;
        }
    }

    if (decision->thread == nullptr){
        delete decision;
        decision = nullptr;
    }

    return decision;
}


void PriorityScheduler::enqueue(const Event* event, Thread* thread) {
    // move to the next queue 
    int priority = thread->process->type;
    queues[priority].emplace(thread);
}


bool PriorityScheduler::should_preempt_on_arrival(const Event* event) const {
    // TODO: implement me
    return false;
}


size_t PriorityScheduler::size() const {
    int size = 0;
    for (int i = 0; i < 4; i++){
        size += queues[i].size();
    }

    return size;
}

string PriorityScheduler::queue_counts(){
    string counts;
    format fmt("[S:%d I:%d N:%d B:%d]");

    counts = (fmt % queues[0].size()
                   % queues[1].size()
                   % queues[2].size()
                   % queues[3].size()).str();

    return counts;
}